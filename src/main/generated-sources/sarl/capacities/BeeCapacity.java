package capacities;

import Java.Solucion;
import io.sarl.lang.annotation.SarlElementType;
import io.sarl.lang.annotation.SarlSpecification;
import io.sarl.lang.core.AgentTrait;
import io.sarl.lang.core.Capacity;

/**
 * @author Matías
 */
@FunctionalInterface
@SarlSpecification("0.8")
@SarlElementType(19)
@SuppressWarnings("all")
public interface BeeCapacity extends Capacity {
  public abstract void waggleDance(final Solucion solucion);
  
  /**
   * @ExcludeFromApidoc
   */
  public static class ContextAwareCapacityWrapper<C extends BeeCapacity> extends Capacity.ContextAwareCapacityWrapper<C> implements BeeCapacity {
    public ContextAwareCapacityWrapper(final C capacity, final AgentTrait caller) {
      super(capacity, caller);
    }
    
    public void waggleDance(final Solucion solucion) {
      try {
        ensureCallerInLocalThread();
        this.capacity.waggleDance(solucion);
      } finally {
        resetCallerInLocalThread();
      }
    }
  }
}
