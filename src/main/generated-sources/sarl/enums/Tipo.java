package enums;

import io.sarl.lang.annotation.SarlElementType;
import io.sarl.lang.annotation.SarlSpecification;

/**
 * @author Matías
 */
@SarlSpecification("0.8")
@SarlElementType(12)
@SuppressWarnings("all")
public enum Tipo {
  UNCOMMITED,
  
  RECRUITER;
}
