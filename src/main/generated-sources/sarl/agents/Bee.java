package agents;

import Java.Solucion;
import Skills.RecruiterSkill;
import Skills.UncommitedSkill;
import capacities.BeeCapacity;
import com.google.common.base.Objects;
import enums.Tipo;
import events.BackwardPass;
import events.FinishedForwardPass;
import events.FordwardPass;
import events.WaggleDance;
import io.sarl.core.Behaviors;
import io.sarl.core.DefaultContextInteractions;
import io.sarl.core.ExternalContextAccess;
import io.sarl.core.Initialize;
import io.sarl.core.Lifecycle;
import io.sarl.core.Logging;
import io.sarl.core.Schedules;
import io.sarl.lang.annotation.ImportedCapacityFeature;
import io.sarl.lang.annotation.PerceptGuardEvaluator;
import io.sarl.lang.annotation.SarlElementType;
import io.sarl.lang.annotation.SarlSpecification;
import io.sarl.lang.annotation.SyntheticMember;
import io.sarl.lang.core.Agent;
import io.sarl.lang.core.BuiltinCapacitiesProvider;
import io.sarl.lang.core.DynamicSkillProvider;
import io.sarl.lang.core.Skill;
import io.sarl.lang.util.ClearableReference;
import io.sarl.util.OpenEventSpace;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.function.BinaryOperator;
import javax.inject.Inject;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.Inline;
import org.eclipse.xtext.xbase.lib.Pure;

/**
 * @author Matías
 */
@SarlSpecification("0.8")
@SarlElementType(18)
@SuppressWarnings("all")
public class Bee extends Agent {
  private Solucion solucion;
  
  private int numero;
  
  private OpenEventSpace hive;
  
  private UUID spaceId;
  
  private final List<Float> knownnectaramounts = new ArrayList<Float>();
  
  private Tipo tipo;
  
  private int iteracion;
  
  private int recruiters;
  
  private BeeCapacity theSkill;
  
  private void $behaviorUnit$Initialize$0(final Initialize occurrence) {
    this.iteracion = 0;
    for (final Object o : occurrence.parameters) {
      {
        Logging _$CAPACITY_USE$IO_SARL_CORE_LOGGING$CALLER = this.$castSkill(Logging.class, (this.$CAPACITY_USE$IO_SARL_CORE_LOGGING == null || this.$CAPACITY_USE$IO_SARL_CORE_LOGGING.get() == null) ? (this.$CAPACITY_USE$IO_SARL_CORE_LOGGING = this.$getSkill(Logging.class)) : this.$CAPACITY_USE$IO_SARL_CORE_LOGGING);
        _$CAPACITY_USE$IO_SARL_CORE_LOGGING$CALLER.info(o);
        if ((o instanceof Solucion)) {
          this.solucion = ((Solucion) o);
          Logging _$CAPACITY_USE$IO_SARL_CORE_LOGGING$CALLER_1 = this.$castSkill(Logging.class, (this.$CAPACITY_USE$IO_SARL_CORE_LOGGING == null || this.$CAPACITY_USE$IO_SARL_CORE_LOGGING.get() == null) ? (this.$CAPACITY_USE$IO_SARL_CORE_LOGGING = this.$getSkill(Logging.class)) : this.$CAPACITY_USE$IO_SARL_CORE_LOGGING);
          _$CAPACITY_USE$IO_SARL_CORE_LOGGING$CALLER_1.info((("BZZZZZZ" + this.solucion) + "BZZZZZZ"));
        }
        if ((o instanceof Integer)) {
          this.numero = ((Integer) o).intValue();
        }
      }
    }
  }
  
  private void $behaviorUnit$FordwardPass$1(final FordwardPass occurrence) {
    this.iteracion = occurrence.iteracion;
    synchronized (this.solucion) {
      Logging _$CAPACITY_USE$IO_SARL_CORE_LOGGING$CALLER = this.$castSkill(Logging.class, (this.$CAPACITY_USE$IO_SARL_CORE_LOGGING == null || this.$CAPACITY_USE$IO_SARL_CORE_LOGGING.get() == null) ? (this.$CAPACITY_USE$IO_SARL_CORE_LOGGING = this.$getSkill(Logging.class)) : this.$CAPACITY_USE$IO_SARL_CORE_LOGGING);
      float _nectarAmount = this.solucion.nectarAmount();
      String _plus = ((("Bee " + Integer.valueOf(this.numero)) + ": My source\'s nectar amount is :") + Float.valueOf(_nectarAmount));
      _$CAPACITY_USE$IO_SARL_CORE_LOGGING$CALLER.info(_plus);
      Solucion newSource = new Solucion(this.solucion);
      Logging _$CAPACITY_USE$IO_SARL_CORE_LOGGING$CALLER_1 = this.$castSkill(Logging.class, (this.$CAPACITY_USE$IO_SARL_CORE_LOGGING == null || this.$CAPACITY_USE$IO_SARL_CORE_LOGGING.get() == null) ? (this.$CAPACITY_USE$IO_SARL_CORE_LOGGING = this.$getSkill(Logging.class)) : this.$CAPACITY_USE$IO_SARL_CORE_LOGGING);
      float _nectarAmount_1 = newSource.nectarAmount();
      String _plus_1 = ((("Bee " + Integer.valueOf(this.numero)) + ": The new source\'s nectar amount is : ") + Float.valueOf(_nectarAmount_1));
      _$CAPACITY_USE$IO_SARL_CORE_LOGGING$CALLER_1.info(_plus_1);
      float _nectarAmount_2 = this.solucion.nectarAmount();
      float _nectarAmount_3 = newSource.nectarAmount();
      boolean _lessThan = (_nectarAmount_2 < _nectarAmount_3);
      if (_lessThan) {
        this.solucion = newSource;
        Logging _$CAPACITY_USE$IO_SARL_CORE_LOGGING$CALLER_2 = this.$castSkill(Logging.class, (this.$CAPACITY_USE$IO_SARL_CORE_LOGGING == null || this.$CAPACITY_USE$IO_SARL_CORE_LOGGING.get() == null) ? (this.$CAPACITY_USE$IO_SARL_CORE_LOGGING = this.$getSkill(Logging.class)) : this.$CAPACITY_USE$IO_SARL_CORE_LOGGING);
        _$CAPACITY_USE$IO_SARL_CORE_LOGGING$CALLER_2.info((("Bee " + Integer.valueOf(this.numero)) + ": So I decided to change to the new source"));
      } else {
        Logging _$CAPACITY_USE$IO_SARL_CORE_LOGGING$CALLER_3 = this.$castSkill(Logging.class, (this.$CAPACITY_USE$IO_SARL_CORE_LOGGING == null || this.$CAPACITY_USE$IO_SARL_CORE_LOGGING.get() == null) ? (this.$CAPACITY_USE$IO_SARL_CORE_LOGGING = this.$getSkill(Logging.class)) : this.$CAPACITY_USE$IO_SARL_CORE_LOGGING);
        _$CAPACITY_USE$IO_SARL_CORE_LOGGING$CALLER_3.info((("Bee " + Integer.valueOf(this.numero)) + ": So I decided to stay like this"));
      }
    }
    this.knownnectaramounts.add(Float.valueOf(this.solucion.nectarAmount()));
    float _nextFloat = new Random().nextFloat();
    float rand = ((float) _nextFloat);
    float loy = this.loyaltyDecision(this.knownnectaramounts);
    if ((rand < loy)) {
      this.tipo = Tipo.RECRUITER;
      RecruiterSkill _recruiterSkill = new RecruiterSkill();
      this.<RecruiterSkill>setSkill(_recruiterSkill, BeeCapacity.class);
      Logging _$CAPACITY_USE$IO_SARL_CORE_LOGGING$CALLER = this.$castSkill(Logging.class, (this.$CAPACITY_USE$IO_SARL_CORE_LOGGING == null || this.$CAPACITY_USE$IO_SARL_CORE_LOGGING.get() == null) ? (this.$CAPACITY_USE$IO_SARL_CORE_LOGGING = this.$getSkill(Logging.class)) : this.$CAPACITY_USE$IO_SARL_CORE_LOGGING);
      _$CAPACITY_USE$IO_SARL_CORE_LOGGING$CALLER.info(((((("Bee " + Integer.valueOf(this.numero)) + ": I became recruiter ") + Float.valueOf(rand)) + "<") + Float.valueOf(loy)));
    } else {
      UncommitedSkill _uncommitedSkill = new UncommitedSkill();
      this.<UncommitedSkill>setSkill(_uncommitedSkill, BeeCapacity.class);
      Logging _$CAPACITY_USE$IO_SARL_CORE_LOGGING$CALLER_1 = this.$castSkill(Logging.class, (this.$CAPACITY_USE$IO_SARL_CORE_LOGGING == null || this.$CAPACITY_USE$IO_SARL_CORE_LOGGING.get() == null) ? (this.$CAPACITY_USE$IO_SARL_CORE_LOGGING = this.$getSkill(Logging.class)) : this.$CAPACITY_USE$IO_SARL_CORE_LOGGING);
      _$CAPACITY_USE$IO_SARL_CORE_LOGGING$CALLER_1.info(((((("Bee " + Integer.valueOf(this.numero)) + ": I became UNCOMMITED ") + Float.valueOf(rand)) + ">") + Float.valueOf(loy)));
      this.tipo = Tipo.UNCOMMITED;
    }
    this.knownnectaramounts.clear();
    DefaultContextInteractions _$CAPACITY_USE$IO_SARL_CORE_DEFAULTCONTEXTINTERACTIONS$CALLER = this.$castSkill(DefaultContextInteractions.class, (this.$CAPACITY_USE$IO_SARL_CORE_DEFAULTCONTEXTINTERACTIONS == null || this.$CAPACITY_USE$IO_SARL_CORE_DEFAULTCONTEXTINTERACTIONS.get() == null) ? (this.$CAPACITY_USE$IO_SARL_CORE_DEFAULTCONTEXTINTERACTIONS = this.$getSkill(DefaultContextInteractions.class)) : this.$CAPACITY_USE$IO_SARL_CORE_DEFAULTCONTEXTINTERACTIONS);
    FinishedForwardPass _finishedForwardPass = new FinishedForwardPass(this.tipo);
    _$CAPACITY_USE$IO_SARL_CORE_DEFAULTCONTEXTINTERACTIONS$CALLER.emit(_finishedForwardPass);
  }
  
  private void $behaviorUnit$BackwardPass$2(final BackwardPass occurrence) {
    throw new Error("Unresolved compilation problems:"
      + "\nType mismatch: cannot convert from float to Solucion");
  }
  
  private void $behaviorUnit$WaggleDance$3(final WaggleDance occurrence) {
    throw new Error("Unresolved compilation problems:"
      + "\nThe method or field nectar is undefined for the type WaggleDance");
  }
  
  @SyntheticMember
  @Pure
  private boolean $behaviorUnitGuard$WaggleDance$3(final WaggleDance it, final WaggleDance occurrence) {
    boolean _equals = Objects.equal(this.tipo, Tipo.UNCOMMITED);
    return _equals;
  }
  
  private void $behaviorUnit$WaggleDance$4(final WaggleDance occurrence) {
    throw new Error("Unresolved compilation problems:"
      + "\nThe method or field nectar is undefined for the type WaggleDance");
  }
  
  @SyntheticMember
  @Pure
  private boolean $behaviorUnitGuard$WaggleDance$4(final WaggleDance it, final WaggleDance occurrence) {
    boolean _equals = Objects.equal(this.tipo, Tipo.RECRUITER);
    return _equals;
  }
  
  protected float loyaltyDecision(final List<Float> nectars) {
    Float max = Collections.<Float>max(nectars);
    Float min = Collections.<Float>min(nectars);
    float ob = 0;
    boolean _equals = Objects.equal(max, min);
    if (_equals) {
      ob = 1;
    } else {
      ob = this.normalize(this.solucion.nectarAmount(), (min).floatValue(), (max).floatValue());
    }
    Logging _$CAPACITY_USE$IO_SARL_CORE_LOGGING$CALLER = this.$castSkill(Logging.class, (this.$CAPACITY_USE$IO_SARL_CORE_LOGGING == null || this.$CAPACITY_USE$IO_SARL_CORE_LOGGING.get() == null) ? (this.$CAPACITY_USE$IO_SARL_CORE_LOGGING = this.$getSkill(Logging.class)) : this.$CAPACITY_USE$IO_SARL_CORE_LOGGING);
    _$CAPACITY_USE$IO_SARL_CORE_LOGGING$CALLER.info(Float.valueOf(ob));
    double _pow = Math.pow(((float) Math.E), ((1 - ob) / (this.iteracion + 1)));
    return ((float) _pow);
  }
  
  protected float recruitingProcess(final List<Float> nectars) {
    Float max = Collections.<Float>max(nectars);
    Float min = Collections.<Float>min(nectars);
    float ob = 0;
    boolean _equals = Objects.equal(max, min);
    if (_equals) {
      ob = 1;
    } else {
      ob = this.normalize(this.solucion.nectarAmount(), (min).floatValue(), (max).floatValue());
    }
    final BinaryOperator<Float> _function = (Float a, Float b) -> {
      float _normalize = this.normalize((b).floatValue(), (min).floatValue(), (max).floatValue());
      return Float.valueOf(((a).floatValue() + _normalize));
    };
    Float sum = Arrays.<Float>stream(((Float[])Conversions.unwrapArray(nectars, Float.class))).reduce(Float.valueOf(0f), _function);
    return (ob / ((float) (sum).floatValue()));
  }
  
  @Pure
  protected float normalize(final float value, final float min, final float max) {
    return ((value - min) / (max - min));
  }
  
  @Extension
  @ImportedCapacityFeature(Logging.class)
  @SyntheticMember
  private transient ClearableReference<Skill> $CAPACITY_USE$IO_SARL_CORE_LOGGING;
  
  @SyntheticMember
  @Pure
  @Inline(value = "$castSkill(Logging.class, ($0$CAPACITY_USE$IO_SARL_CORE_LOGGING == null || $0$CAPACITY_USE$IO_SARL_CORE_LOGGING.get() == null) ? ($0$CAPACITY_USE$IO_SARL_CORE_LOGGING = $0$getSkill(Logging.class)) : $0$CAPACITY_USE$IO_SARL_CORE_LOGGING)", imported = Logging.class)
  private Logging $CAPACITY_USE$IO_SARL_CORE_LOGGING$CALLER() {
    if (this.$CAPACITY_USE$IO_SARL_CORE_LOGGING == null || this.$CAPACITY_USE$IO_SARL_CORE_LOGGING.get() == null) {
      this.$CAPACITY_USE$IO_SARL_CORE_LOGGING = $getSkill(Logging.class);
    }
    return $castSkill(Logging.class, this.$CAPACITY_USE$IO_SARL_CORE_LOGGING);
  }
  
  @Extension
  @ImportedCapacityFeature(Lifecycle.class)
  @SyntheticMember
  private transient ClearableReference<Skill> $CAPACITY_USE$IO_SARL_CORE_LIFECYCLE;
  
  @SyntheticMember
  @Pure
  @Inline(value = "$castSkill(Lifecycle.class, ($0$CAPACITY_USE$IO_SARL_CORE_LIFECYCLE == null || $0$CAPACITY_USE$IO_SARL_CORE_LIFECYCLE.get() == null) ? ($0$CAPACITY_USE$IO_SARL_CORE_LIFECYCLE = $0$getSkill(Lifecycle.class)) : $0$CAPACITY_USE$IO_SARL_CORE_LIFECYCLE)", imported = Lifecycle.class)
  private Lifecycle $CAPACITY_USE$IO_SARL_CORE_LIFECYCLE$CALLER() {
    if (this.$CAPACITY_USE$IO_SARL_CORE_LIFECYCLE == null || this.$CAPACITY_USE$IO_SARL_CORE_LIFECYCLE.get() == null) {
      this.$CAPACITY_USE$IO_SARL_CORE_LIFECYCLE = $getSkill(Lifecycle.class);
    }
    return $castSkill(Lifecycle.class, this.$CAPACITY_USE$IO_SARL_CORE_LIFECYCLE);
  }
  
  @Extension
  @ImportedCapacityFeature(Schedules.class)
  @SyntheticMember
  private transient ClearableReference<Skill> $CAPACITY_USE$IO_SARL_CORE_SCHEDULES;
  
  @SyntheticMember
  @Pure
  @Inline(value = "$castSkill(Schedules.class, ($0$CAPACITY_USE$IO_SARL_CORE_SCHEDULES == null || $0$CAPACITY_USE$IO_SARL_CORE_SCHEDULES.get() == null) ? ($0$CAPACITY_USE$IO_SARL_CORE_SCHEDULES = $0$getSkill(Schedules.class)) : $0$CAPACITY_USE$IO_SARL_CORE_SCHEDULES)", imported = Schedules.class)
  private Schedules $CAPACITY_USE$IO_SARL_CORE_SCHEDULES$CALLER() {
    if (this.$CAPACITY_USE$IO_SARL_CORE_SCHEDULES == null || this.$CAPACITY_USE$IO_SARL_CORE_SCHEDULES.get() == null) {
      this.$CAPACITY_USE$IO_SARL_CORE_SCHEDULES = $getSkill(Schedules.class);
    }
    return $castSkill(Schedules.class, this.$CAPACITY_USE$IO_SARL_CORE_SCHEDULES);
  }
  
  @Extension
  @ImportedCapacityFeature(DefaultContextInteractions.class)
  @SyntheticMember
  private transient ClearableReference<Skill> $CAPACITY_USE$IO_SARL_CORE_DEFAULTCONTEXTINTERACTIONS;
  
  @SyntheticMember
  @Pure
  @Inline(value = "$castSkill(DefaultContextInteractions.class, ($0$CAPACITY_USE$IO_SARL_CORE_DEFAULTCONTEXTINTERACTIONS == null || $0$CAPACITY_USE$IO_SARL_CORE_DEFAULTCONTEXTINTERACTIONS.get() == null) ? ($0$CAPACITY_USE$IO_SARL_CORE_DEFAULTCONTEXTINTERACTIONS = $0$getSkill(DefaultContextInteractions.class)) : $0$CAPACITY_USE$IO_SARL_CORE_DEFAULTCONTEXTINTERACTIONS)", imported = DefaultContextInteractions.class)
  private DefaultContextInteractions $CAPACITY_USE$IO_SARL_CORE_DEFAULTCONTEXTINTERACTIONS$CALLER() {
    if (this.$CAPACITY_USE$IO_SARL_CORE_DEFAULTCONTEXTINTERACTIONS == null || this.$CAPACITY_USE$IO_SARL_CORE_DEFAULTCONTEXTINTERACTIONS.get() == null) {
      this.$CAPACITY_USE$IO_SARL_CORE_DEFAULTCONTEXTINTERACTIONS = $getSkill(DefaultContextInteractions.class);
    }
    return $castSkill(DefaultContextInteractions.class, this.$CAPACITY_USE$IO_SARL_CORE_DEFAULTCONTEXTINTERACTIONS);
  }
  
  @Extension
  @ImportedCapacityFeature(Behaviors.class)
  @SyntheticMember
  private transient ClearableReference<Skill> $CAPACITY_USE$IO_SARL_CORE_BEHAVIORS;
  
  @SyntheticMember
  @Pure
  @Inline(value = "$castSkill(Behaviors.class, ($0$CAPACITY_USE$IO_SARL_CORE_BEHAVIORS == null || $0$CAPACITY_USE$IO_SARL_CORE_BEHAVIORS.get() == null) ? ($0$CAPACITY_USE$IO_SARL_CORE_BEHAVIORS = $0$getSkill(Behaviors.class)) : $0$CAPACITY_USE$IO_SARL_CORE_BEHAVIORS)", imported = Behaviors.class)
  private Behaviors $CAPACITY_USE$IO_SARL_CORE_BEHAVIORS$CALLER() {
    if (this.$CAPACITY_USE$IO_SARL_CORE_BEHAVIORS == null || this.$CAPACITY_USE$IO_SARL_CORE_BEHAVIORS.get() == null) {
      this.$CAPACITY_USE$IO_SARL_CORE_BEHAVIORS = $getSkill(Behaviors.class);
    }
    return $castSkill(Behaviors.class, this.$CAPACITY_USE$IO_SARL_CORE_BEHAVIORS);
  }
  
  @Extension
  @ImportedCapacityFeature(ExternalContextAccess.class)
  @SyntheticMember
  private transient ClearableReference<Skill> $CAPACITY_USE$IO_SARL_CORE_EXTERNALCONTEXTACCESS;
  
  @SyntheticMember
  @Pure
  @Inline(value = "$castSkill(ExternalContextAccess.class, ($0$CAPACITY_USE$IO_SARL_CORE_EXTERNALCONTEXTACCESS == null || $0$CAPACITY_USE$IO_SARL_CORE_EXTERNALCONTEXTACCESS.get() == null) ? ($0$CAPACITY_USE$IO_SARL_CORE_EXTERNALCONTEXTACCESS = $0$getSkill(ExternalContextAccess.class)) : $0$CAPACITY_USE$IO_SARL_CORE_EXTERNALCONTEXTACCESS)", imported = ExternalContextAccess.class)
  private ExternalContextAccess $CAPACITY_USE$IO_SARL_CORE_EXTERNALCONTEXTACCESS$CALLER() {
    if (this.$CAPACITY_USE$IO_SARL_CORE_EXTERNALCONTEXTACCESS == null || this.$CAPACITY_USE$IO_SARL_CORE_EXTERNALCONTEXTACCESS.get() == null) {
      this.$CAPACITY_USE$IO_SARL_CORE_EXTERNALCONTEXTACCESS = $getSkill(ExternalContextAccess.class);
    }
    return $castSkill(ExternalContextAccess.class, this.$CAPACITY_USE$IO_SARL_CORE_EXTERNALCONTEXTACCESS);
  }
  
  @Extension
  @ImportedCapacityFeature(BeeCapacity.class)
  @SyntheticMember
  private transient ClearableReference<Skill> $CAPACITY_USE$CAPACITIES_BEECAPACITY;
  
  @SyntheticMember
  @Pure
  @Inline(value = "$castSkill(BeeCapacity.class, ($0$CAPACITY_USE$CAPACITIES_BEECAPACITY == null || $0$CAPACITY_USE$CAPACITIES_BEECAPACITY.get() == null) ? ($0$CAPACITY_USE$CAPACITIES_BEECAPACITY = $0$getSkill(BeeCapacity.class)) : $0$CAPACITY_USE$CAPACITIES_BEECAPACITY)", imported = BeeCapacity.class)
  private BeeCapacity $CAPACITY_USE$CAPACITIES_BEECAPACITY$CALLER() {
    if (this.$CAPACITY_USE$CAPACITIES_BEECAPACITY == null || this.$CAPACITY_USE$CAPACITIES_BEECAPACITY.get() == null) {
      this.$CAPACITY_USE$CAPACITIES_BEECAPACITY = $getSkill(BeeCapacity.class);
    }
    return $castSkill(BeeCapacity.class, this.$CAPACITY_USE$CAPACITIES_BEECAPACITY);
  }
  
  @SyntheticMember
  @PerceptGuardEvaluator
  private void $guardEvaluator$Initialize(final Initialize occurrence, final Collection<Runnable> ___SARLlocal_runnableCollection) {
    assert occurrence != null;
    assert ___SARLlocal_runnableCollection != null;
    ___SARLlocal_runnableCollection.add(() -> $behaviorUnit$Initialize$0(occurrence));
  }
  
  @SyntheticMember
  @PerceptGuardEvaluator
  private void $guardEvaluator$WaggleDance(final WaggleDance occurrence, final Collection<Runnable> ___SARLlocal_runnableCollection) {
    assert occurrence != null;
    assert ___SARLlocal_runnableCollection != null;
    if ($behaviorUnitGuard$WaggleDance$3(occurrence, occurrence)) {
      ___SARLlocal_runnableCollection.add(() -> $behaviorUnit$WaggleDance$3(occurrence));
    }
    if ($behaviorUnitGuard$WaggleDance$4(occurrence, occurrence)) {
      ___SARLlocal_runnableCollection.add(() -> $behaviorUnit$WaggleDance$4(occurrence));
    }
  }
  
  @SyntheticMember
  @PerceptGuardEvaluator
  private void $guardEvaluator$BackwardPass(final BackwardPass occurrence, final Collection<Runnable> ___SARLlocal_runnableCollection) {
    assert occurrence != null;
    assert ___SARLlocal_runnableCollection != null;
    ___SARLlocal_runnableCollection.add(() -> $behaviorUnit$BackwardPass$2(occurrence));
  }
  
  @SyntheticMember
  @PerceptGuardEvaluator
  private void $guardEvaluator$FordwardPass(final FordwardPass occurrence, final Collection<Runnable> ___SARLlocal_runnableCollection) {
    assert occurrence != null;
    assert ___SARLlocal_runnableCollection != null;
    ___SARLlocal_runnableCollection.add(() -> $behaviorUnit$FordwardPass$1(occurrence));
  }
  
  @Override
  @Pure
  @SyntheticMember
  public boolean equals(final Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Bee other = (Bee) obj;
    if (other.numero != this.numero)
      return false;
    if (!java.util.Objects.equals(this.spaceId, other.spaceId)) {
      return false;
    }
    if (other.iteracion != this.iteracion)
      return false;
    if (other.recruiters != this.recruiters)
      return false;
    return super.equals(obj);
  }
  
  @Override
  @Pure
  @SyntheticMember
  public int hashCode() {
    int result = super.hashCode();
    final int prime = 31;
    result = prime * result + this.numero;
    result = prime * result + java.util.Objects.hashCode(this.spaceId);
    result = prime * result + this.iteracion;
    result = prime * result + this.recruiters;
    return result;
  }
  
  @SyntheticMember
  public Bee(final UUID parentID, final UUID agentID) {
    super(parentID, agentID);
  }
  
  @SyntheticMember
  @Inject
  @Deprecated
  public Bee(final BuiltinCapacitiesProvider provider, final UUID parentID, final UUID agentID) {
    super(provider, parentID, agentID);
  }
  
  @SyntheticMember
  @Inject
  public Bee(final UUID parentID, final UUID agentID, final DynamicSkillProvider skillProvider) {
    super(parentID, agentID, skillProvider);
  }
}
