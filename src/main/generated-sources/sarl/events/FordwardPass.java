package events;

import io.sarl.lang.annotation.SarlElementType;
import io.sarl.lang.annotation.SarlSpecification;
import io.sarl.lang.annotation.SyntheticMember;
import io.sarl.lang.core.Event;
import org.eclipse.xtext.xbase.lib.Pure;
import org.eclipse.xtext.xbase.lib.util.ToStringBuilder;

/**
 * @author Matías
 */
@SarlSpecification("0.8")
@SarlElementType(15)
@SuppressWarnings("all")
public class FordwardPass extends Event {
  public int iteracion;
  
  public FordwardPass(final int i) {
    this.iteracion = i;
  }
  
  @Override
  @Pure
  @SyntheticMember
  public boolean equals(final Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    FordwardPass other = (FordwardPass) obj;
    if (other.iteracion != this.iteracion)
      return false;
    return super.equals(obj);
  }
  
  @Override
  @Pure
  @SyntheticMember
  public int hashCode() {
    int result = super.hashCode();
    final int prime = 31;
    result = prime * result + this.iteracion;
    return result;
  }
  
  /**
   * Returns a String representation of the FordwardPass event's attributes only.
   */
  @SyntheticMember
  @Pure
  protected void toString(final ToStringBuilder builder) {
    super.toString(builder);
    builder.add("iteracion", this.iteracion);
  }
  
  @SyntheticMember
  private final static long serialVersionUID = -1404958445L;
}
