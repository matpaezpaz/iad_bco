package events;

import io.sarl.lang.annotation.SarlElementType;
import io.sarl.lang.annotation.SarlSpecification;
import io.sarl.lang.annotation.SyntheticMember;
import io.sarl.lang.core.Event;
import org.eclipse.xtext.xbase.lib.Pure;
import org.eclipse.xtext.xbase.lib.util.ToStringBuilder;

/**
 * @author Matías
 */
@SarlSpecification("0.8")
@SarlElementType(15)
@SuppressWarnings("all")
public class BackwardPass extends Event {
  public int iteracion;
  
  public int recruiters;
  
  public BackwardPass(final int i, final int recruiters) {
    String lock = "";
    this.iteracion = i;
    this.recruiters = recruiters;
  }
  
  @Override
  @Pure
  @SyntheticMember
  public boolean equals(final Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    BackwardPass other = (BackwardPass) obj;
    if (other.iteracion != this.iteracion)
      return false;
    if (other.recruiters != this.recruiters)
      return false;
    return super.equals(obj);
  }
  
  @Override
  @Pure
  @SyntheticMember
  public int hashCode() {
    int result = super.hashCode();
    final int prime = 31;
    result = prime * result + this.iteracion;
    result = prime * result + this.recruiters;
    return result;
  }
  
  /**
   * Returns a String representation of the BackwardPass event's attributes only.
   */
  @SyntheticMember
  @Pure
  protected void toString(final ToStringBuilder builder) {
    super.toString(builder);
    builder.add("iteracion", this.iteracion);
    builder.add("recruiters", this.recruiters);
  }
  
  @SyntheticMember
  private final static long serialVersionUID = -2110571721L;
}
