package events;

import io.sarl.lang.annotation.SarlElementType;
import io.sarl.lang.annotation.SarlSpecification;
import io.sarl.lang.annotation.SyntheticMember;
import io.sarl.lang.core.Address;
import io.sarl.lang.core.Event;

/**
 * @author Matías
 */
@SarlSpecification("0.8")
@SarlElementType(15)
@SuppressWarnings("all")
public class KillBees extends Event {
  @SyntheticMember
  public KillBees() {
    super();
  }
  
  @SyntheticMember
  public KillBees(final Address source) {
    super(source);
  }
  
  @SyntheticMember
  private final static long serialVersionUID = 588368462L;
}
