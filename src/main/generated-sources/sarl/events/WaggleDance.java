package events;

import Java.Solucion;
import io.sarl.lang.annotation.SarlElementType;
import io.sarl.lang.annotation.SarlSpecification;
import io.sarl.lang.annotation.SyntheticMember;
import io.sarl.lang.core.Event;
import org.eclipse.xtext.xbase.lib.Pure;
import org.eclipse.xtext.xbase.lib.util.ToStringBuilder;

/**
 * @author Matías
 */
@SarlSpecification("0.8")
@SarlElementType(15)
@SuppressWarnings("all")
public class WaggleDance extends Event {
  public Solucion solucion;
  
  public WaggleDance(final Solucion solucion) {
    this.solucion = solucion;
  }
  
  @Override
  @Pure
  @SyntheticMember
  public boolean equals(final Object obj) {
    return super.equals(obj);
  }
  
  @Override
  @Pure
  @SyntheticMember
  public int hashCode() {
    int result = super.hashCode();
    return result;
  }
  
  /**
   * Returns a String representation of the WaggleDance event's attributes only.
   */
  @SyntheticMember
  @Pure
  protected void toString(final ToStringBuilder builder) {
    super.toString(builder);
    builder.add("solucion", this.solucion);
  }
  
  @SyntheticMember
  private final static long serialVersionUID = 1261312143L;
}
