package Java;
public class Bound {
	float min,max;
	public Bound (float min, float max) {
		this.min = min;
		this.max = max;
	}
	float getMin(){
		return this.min;
	}

	float getMax() {
		return this.max;
	}
	float getTenP() {
		return (this.max - this.min) * 0.1f;
	}
}