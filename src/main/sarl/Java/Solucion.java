package Java;
import java.util.List;
import java.util.Random;
import java.util.ArrayList;

public class Solucion {
	List<Float> coord  = new ArrayList<Float>();
	List<Bound> bounds = new ArrayList<Bound>();
	//Crea una solución aleatoria 
	public Solucion (List<Bound> bounds) {
		this.bounds = bounds;
		bounds.forEach( p -> coord.add(aleatorioEntre(p.min,p.max)));
	}
	//Crea una nueva solucion modificandose a sí misma
	public Solucion (Solucion sol){
		this.coord.addAll(sol.coord);
		this.bounds = sol.bounds;
		//TODO
		//cambiar para que elija muchas 
		int elegirCoord = aleatorioEntreInt(0,sol.coord.size());
		List<Integer> coordenadasAModificar = new ArrayList<Integer>();
		coordenadasAModificar.add(elegirCoord);
		for ( Integer i : coordenadasAModificar){
			float dist = aleatorioEntre(0, this.bounds.get(i).getTenP());
			if ( new Random().nextFloat() < 0.5f) {
				//se mueve al max
				float valor = this.coord.get(i) + dist;
				if ( valor > this.bounds.get(i).max ) {
					this.coord.set ( i , this.bounds.get(i).max);
				} else {
					this.coord.set(i, valor);
				}
			} else {
				// se mueve al min
				float valor = this.coord.get(i) - dist;
				if (valor < this.bounds.get(i).min) {
					this.coord.set(i, this.bounds.get(i).min);
				} else {
					this.coord.set(i, valor);
				}
			}
		}
	}
	
	float aleatorioEntre(float min, float max) {
		return ((new Random()).nextFloat() * (max - min)) + min;
	}

	int aleatorioEntreInt(int min, int max) {
		return (new Random()).nextInt(max - min) + min;
	}
	
	@Override
	public String toString() {
		String cadena = "[" + coord.get(0);
		for (int i = 1; i < coord.size(); i++ ) {
			cadena += ","+ coord.get(i);
		}
		cadena += "]";
		return cadena;
	}
	public float nectarAmount(){
		float nectar;
		nectar = 0f;
		nectar = coord.stream().reduce(0f, (a,b) -> a + b);
		return nectar;
	}
	
}